import javax.swing.JOptionPane;
public class CalculateNumbers {
	public static void main(String[] args){
		String strNum1, strNum2;
		String strNotification = "You've just entered: ";
		strNum1 = JOptionPane.showInputDialog(null, 
						"Please input the first number: ", "Input the first number", 							JOptionPane.INFORMATION_MESSAGE);
		strNum2 = JOptionPane.showInputDialog(null, 
						"Please input the second number: ", "Input the second number", 							JOptionPane.INFORMATION_MESSAGE);
		double num1 = Double.parseDouble(strNum1);
		double num2 = Double.parseDouble(strNum2);
		double sum = num1 + num2;
		JOptionPane.showMessageDialog(null, sum, "Sum", JOptionPane.INFORMATION_MESSAGE);
		double diff = num1 - num2;
		JOptionPane.showMessageDialog(null, diff, "Difference", JOptionPane.INFORMATION_MESSAGE);
		double product = num1*num2;
		JOptionPane.showMessageDialog(null, product, "Product", JOptionPane.INFORMATION_MESSAGE);
		double quotien;
		if (num2 == 0) JOptionPane.showMessageDialog(null, "Error num2=0", "Quotien",
						JOptionPane.INFORMATION_MESSAGE);
		else 
		{
			quotien = num1/num2;
			JOptionPane.showMessageDialog(null, quotien, "Quotien", JOptionPane.INFORMATION_MESSAGE);
		}
		System.exit(0);
	}
}

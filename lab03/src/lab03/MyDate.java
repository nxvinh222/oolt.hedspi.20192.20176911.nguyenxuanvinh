import java.util.*;
import java.text.SimpleDateFormat;  
import java.util.Date; 
public class MyDate {
	private int month;
	private int year;
	private int day;
	public int getMonth() {
		return this.month;
	}
	public void setMonth(int month) {
		if (month < 13 && month > 0)
		this.month = month;
	}
	public int getYear() {
		return this.year;
	}
	public void setYear(int year) {
		if(year > 0)
		this.year = year;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if(this.month == 2) {
			if(this.year%4 == 0)
			{
				if(year%100 == 0)
				{if(year%400 == 0)
				{
					if(day > 0 && day < 30)
						this.day = day;
				} else {
					if(day > 0 && day < 29)
						this.day = day;
				}
				}
			}
		}
		if(this.month == 1 || this.month == 3 ||this.month == 5 || 
				this.month == 7|| this.month == 8 || this.month == 10 || 
				this.month == 12)
{
		if(day>0 && day < 32) this.day = day;
	}
		if (this.month == 4 || this.month == 6 || this.month == 9 || this.month == 11)
		{if(day>0 && day<31) this.day = day;
	}
	}
	public void nonPara() {
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		day = calendar.get(Calendar.DATE);
		month = calendar.get(Calendar.MONTH) + 1;
		year = calendar.get(Calendar.YEAR);
	}
	public void threePara(int year, int month, int day) {
		this.day = day;
		this.month = month;
		this.year = year;
		
	}
	
	public void accept() {
		String s; 
		Scanner sc = new Scanner(System.in);
 		System.out.println("Enter a date(YYYY/MM/DD)"); 
 		s = sc.nextLine();  
 		
 		String strY = s.substring(0, 4);
		String strM = s.substring(5, 7);
		String strD = s.substring(8, 10);
		
		int year = Integer.parseInt(strY);
		int month = Integer.parseInt(strM);
		int day = Integer.parseInt(strD);
		setYear(year);
		setMonth(month);
		setDay(day);

		
	}
	public void printCurrentDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	    Date date = new Date();  
	    System.out.println(formatter.format(date)); 
	}
	public void printDate() {
		System.out.println(+this.year+"/"+this.month+"/"+this.day);
	}
	
}

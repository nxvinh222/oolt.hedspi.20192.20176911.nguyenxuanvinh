
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered = 0;
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered < MAX_NUMBERS_ORDERED) {
			
			itemsOrdered[qtyOrdered] = disc;
			qtyOrdered = qtyOrdered+1;
			System.out.println("the disc has been added to the order.");
		} else {
			System.out.println("the order is full");
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int i,j,m;
		m=0;
		for(i=0;i<qtyOrdered;i++) {
			if(itemsOrdered[i] == disc) {
				m=1;
				break;
			}
		}
		if(m == 0) {
			System.out.println("Not found the disc./n");
		}
		else {
		for(j=i;j<qtyOrdered;j++) {
			itemsOrdered[j] = itemsOrdered[j+1];
		}
		qtyOrdered -=1;
	}
	}
	public float totalCost() {
		float sum = 0;
		for(int i = 0;i<qtyOrdered; i++) {
			sum = sum + itemsOrdered[i].getCost();
		}
		return sum;
	}
	
}

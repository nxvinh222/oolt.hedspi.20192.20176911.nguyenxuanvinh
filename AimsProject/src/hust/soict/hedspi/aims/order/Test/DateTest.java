package hust.soict.hedspi.aims.order.Test;
import hust.soict.hedspi.aims.MyDate.MyDate;

public class DateTest {

	public static void main(String[] args) {
		MyDate date1 = new MyDate();
		date1.setYear(1999);
		date1.setMonth(07);
		date1.setDay(10);
		date1.printDate();
		MyDate date2 = new MyDate();
		date2.printCurrentDate();
		MyDate date3= new MyDate();
		date3.accept();
		date3.printDate();
		MyDate date4 = new MyDate();
		date4.threePara(2000,01,01);
		date4.printDate();
		DateUtils.compare(date1, date4);
	}

}

package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media {

//	private String title;
//	private String category;
//	private float cost;
	private List<String> authors = new ArrayList<String>();
	
	
	public Book(String title, String category, int cost) {
		this.title = title;
		this.category = category;
		this.cost = cost;
		// constructor stub
	}
	
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	public void addAuthor(String authorName) {
		//add author
		if (!authors.contains(authorName)) {
			authors.add(authorName);
		}
	}
	
	public void removeAuthor(String authorName) {
		// remove author
		if (authors.contains(authorName)) {
			authors.remove(authorName);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

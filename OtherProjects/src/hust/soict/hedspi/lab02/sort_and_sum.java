package hust.soict.hedspi.lab02;
import java.util.Scanner;
import java.util.Arrays; 


public class sort_and_sum {
	
	static void prt_arr(int [] array) {
		for (int i: array) {
			System.out.println(i);
		}
	}
	
	static int sum(int [] array) {
		int arr_sum = 0;
		
		for (int i: array) {
			arr_sum = arr_sum + i;
		}
		return arr_sum;
	}
	
	static int avg(int [] array) {
		int arr_avg = 0;
		for (int i: array) {
			arr_avg = arr_avg + i;
		}
		
		arr_avg = arr_avg / array.length;
		return arr_avg;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter number of elements");
		int n = keyboard.nextInt();
		
		int arr[] = new int[n];
		
		System.out.println("enter elements");
		
		for (int i = 0; i < n; i++) {
			arr[i] = keyboard.nextInt();
		}
		
		System.out.println("Sorted array is: ");
		prt_arr(arr);
		System.out.println("Sum of array is: " + sum(arr));
		System.out.println("Average of array is: " + avg(arr));
		
		
	}

}

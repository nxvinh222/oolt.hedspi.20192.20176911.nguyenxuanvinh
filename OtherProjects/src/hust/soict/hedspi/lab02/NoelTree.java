package hust.soict.hedspi.lab02;
import java.util.Scanner;

public class NoelTree {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter tree's height:");
		int height = keyboard.nextInt();
		
//		int height = 5;
		int length = 2 * height + 1;
		int middle = length / 2;
		for (int x = 0; x < height; x++) {
			for (int i = 0; i < length; i++) {
				if (i >= middle - x && i <= middle + x) {
					System.out.print("*");
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
			
	}

}

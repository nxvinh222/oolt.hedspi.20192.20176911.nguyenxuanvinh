package hust.soict.hedspi.lab02;

import java.util.Scanner;
import java.util.Arrays; 

public class add_matrix {
	
	
	static void prt_arr(int [][] array) {
//		System.out.println(Arrays.deepToString(array));
		for (int[] x : array)
		{
		   for (int y : x)
		   {
		        System.out.print(y + " ");
		   }
		   System.out.println();
		}
	}
	
	static int[][] plus_mx(int [][] arr1, int [][] arr2) {
		Scanner keyboard = new Scanner(System.in);
		int n = arr1.length;
		int m = arr1[0].length;
		int mx_sum[][] = new int[n][m];
		
		//*******
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				mx_sum[i][j] = arr1[i][j] + arr2[i][j];
			}
		}
		
		return mx_sum;
	}
	

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter rows:");
		int n = keyboard.nextInt();
		System.out.println("Enter columns:");
		int m = keyboard.nextInt();
		
		int mx_1[][] = new int[n][m];
		int mx_2[][] = new int[n][m];
		
		
		System.out.println("enter elements of matrix 1");
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				mx_1[i][j] = keyboard.nextInt();
			}
		}
		
		
		System.out.println("enter elements of matrix 2");
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				mx_2[i][j] = keyboard.nextInt();
			}
		}
		
		
//		prt_arr(mx_1);
//		prt_arr(mx_2);
		System.out.println("Sum of 2 matrixs is ");
		prt_arr(plus_mx(mx_1, mx_2));
		
	}

}

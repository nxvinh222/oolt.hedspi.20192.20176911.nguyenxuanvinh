package hust.soict.hedspi.date;

public class TestPassingParameter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
		DigitalVideoDisc cinderDVD = new DigitalVideoDisc("Cinder");
		
		swap(jungleDVD, cinderDVD);
		System.out.println("jungle dvd title: " + jungleDVD.getTitle());
		System.out.println("cinder dvd title: " + cinderDVD.getTitle());
		
		changeTitle(jungleDVD, cinderDVD.getTitle());
		System.out.println("jungle dvd title: " + jungleDVD.getTitle());
	}

	public static void swap(Object o1, Object o2) {
		Object tmp = o1;
		o1 = o2;
		o2 = tmp;
	}
	
	public static void changeTitle(DigitalVideoDisc dvd, String title) {
		String oldTitle = dvd.getTitle();
		dvd.setTitle(title);
		dvd = new DigitalVideoDisc(oldTitle);
	}
}
